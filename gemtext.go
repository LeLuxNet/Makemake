package makemake

import (
	"strings"
)

const GemtextMimeType = "text/gemini"

func Link(url string, label string) string {
	t := "=> " + url
	if label != "" {
		t += " " + label
	}
	return t
}

func Heading(level int, text string) string {
	return prefixNewlines(strings.Repeat("#", level)+" ", text)
}

func List(lines ...string) string {
	return prefixLines("* ", lines)
}

func Blockquote(text string) string {
	return prefixNewlines("> ", text)
}

func Code(text string) string {
	return "```\n" + text + "\n```"
}

func prefixNewlines(prefix string, text string) string {
	if text == "" {
		return ""
	}

	return prefix + strings.ReplaceAll(text, "\n", "\n"+prefix)
}

func prefixLines(prefix string, lines []string) string {
	if len(lines) == 0 {
		return ""
	}

	return prefix + strings.Join(lines, "\n"+prefix)
}
