package makemake

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLink(t *testing.T) {
	assert.Equal(t, "=> gemini://localhost Local", Link("gemini://localhost", "Local"))

	assert.Equal(t, "=> /relative", Link("/relative", ""))
}

func TestHeading(t *testing.T) {
	assert.Equal(t, "# Book", Heading(1, "Book"))

	assert.Equal(t, "## Chapter", Heading(2, "Chapter"))

	assert.Equal(t, "### Paragraph", Heading(3, "Paragraph"))
}

func TestList(t *testing.T) {
	assert.Equal(t, "", List())

	assert.Equal(t, "* Bread", List("Bread"))

	assert.Equal(t, "* Bread\n* Butter\n* Milk", List("Bread", "Butter", "Milk"))
}

func TestBlockquote(t *testing.T) {
	assert.Equal(t, "> Lorem ipsum", Blockquote("Lorem ipsum"))

	assert.Equal(t, "> Lorem\n> ipsum", Blockquote("Lorem\nipsum"))
}

func TestCode(t *testing.T) {
	assert.Equal(t, "```\necho 1\n```", Code("echo 1"))

	assert.Equal(t, "```\necho 1\necho 2\n```", Code("echo 1\necho 2"))
}
