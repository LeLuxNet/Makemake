package makemake

import (
	"errors"
	"io"
	"net/http"
	"strconv"
	"time"
)

var NoContentTypeHeader = errors.New("abc")

type Handler interface {
	ServeGemini(*Response, *Request)
}

type HandlerFunc func(*Response, *Request)

func (f HandlerFunc) ServeGemini(w *Response, r *Request) {
	f(w, r)
}

func HTTP(w *Response, r *http.Response) error {
	var err error
	if !w.wroteHeader {
		mime := r.Header.Get("Content-Type")
		if mime == "" {
			return NoContentTypeHeader
		}

		err = w.WriteHeader(StatusSuccess, mime)
		if err != nil {
			return err
		}
	}

	_, err = io.Copy(w, r.Body)
	return err
}

func Input(w *Response, prompt string, sensitive bool) error {
	if sensitive {
		return w.WriteHeader(StatusSensitiveInput, prompt)
	} else {
		return w.WriteHeader(StatusInput, prompt)
	}
}

func Redirect(w *Response, url string, temporary bool) error {
	if temporary {
		return w.WriteHeader(StatusRedirectTemporary, url)
	} else {
		return w.WriteHeader(StatusRedirectPermanent, url)
	}
}

func RateLimit(w *Response, d time.Duration) error {
	return w.WriteHeader(StatusSlowDown, strconv.Itoa(int(d/time.Second)))
}
