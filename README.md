# Makemake

Yet another [Gemini](https://gemini.circumlunar.space) server.

## Install

```sh
go get lelux.net/makemake
```

## Example

Generate a self-signed local cert

```sh
openssl req -new -x509 -days 365 -nodes -out cert.pem -keyout key.pem -subj "//CN=localhost" -newkey rsa:4096 -addext "subjectAltName = DNS:localhost,DNS:127.0.0.1"
```

Run the example

```go
package main

import (
	"fmt"
	"net/http"

  "lelux.net/makemake"
)

func handle(w *makemake.Response, r *makemake.Request) {
	fmt.Println(r.URL)

	switch r.URL.Path {
	case "/":
		text :=
			makemake.Heading(1, "Makemake") +
				"\nYet another Gemini server.\n" +
				makemake.Link("/dice", "An image")

		w.WriteString(text)
	case "/dice":
		res, err := http.Get("https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png")
		if err != nil {
			return
		}
		defer res.Body.Close()

		makemake.HTTP(w, res)
	}
}

func main() {
	server := makemake.Server{
		Handler: makemake.HandlerFunc(handle),
	}

	server.ListenAndServeTLS("cert.pem", "key.pem")
}
```
