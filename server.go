package makemake

import (
	"bufio"
	"crypto/tls"
	"io"
	"net"
	"net/url"
	"strconv"
)

const DefaultPort = "1965"

type Request struct {
	URL *url.URL
}

var _ io.Writer = (*Response)(nil)
var _ io.StringWriter = (*Response)(nil)

type Response struct {
	wroteHeader bool

	w *bufio.Writer
}

func (r *Response) WriteHeader(statusCode int, meta string) error {
	_, err := r.w.WriteString(strconv.Itoa(statusCode) + " " + meta + "\r\n")
	if err != nil {
		return err
	}
	r.wroteHeader = true
	return nil
}

func (r *Response) sendHeader() {
	if !r.wroteHeader {
		r.WriteHeader(StatusSuccess, GemtextMimeType)
	}
}

func (r *Response) Write(p []byte) (int, error) {
	r.sendHeader()
	return r.w.Write(p)
}

func (r *Response) WriteString(s string) (int, error) {
	r.sendHeader()
	return r.w.WriteString(s)
}

func (r *Response) Flush() error {
	return r.w.Flush()
}

type Server struct {
	Addr string

	Handler

	TLSConfig *tls.Config
}

func (srv *Server) Serve(l net.Listener) error {
	for {
		c, err := l.Accept()
		if err != nil {
			continue
		}

		go srv.serve(c)
	}
}

func (srv *Server) ServeTLS(l net.Listener, certFile, keyFile string) error {
	var config *tls.Config
	if srv.TLSConfig != nil {
		config = srv.TLSConfig.Clone()
	} else {
		config = &tls.Config{}
	}

	hasCert := len(config.Certificates) > 0 || config.GetCertificate != nil
	if !hasCert || certFile != "" || keyFile != "" {
		config.Certificates = make([]tls.Certificate, 1)

		var err error
		config.Certificates[0], err = tls.LoadX509KeyPair(certFile, keyFile)
		if err != nil {
			return err
		}
	}

	t := tls.NewListener(l, config)
	return srv.Serve(t)
}

func (srv *Server) serve(c net.Conn) {
	header, _, err := bufio.NewReader(c).ReadLine()
	if err != nil {
		return
	}

	var u *url.URL
	u, err = url.Parse(string(header))
	if err != nil {
		return
	}

	res := &Response{w: bufio.NewWriter(c)}
	req := &Request{URL: u}

	srv.Handler.ServeGemini(res, req)

	res.w.Flush()
	c.Close()
}

func (srv *Server) ListenAndServe() error {
	addr := srv.Addr
	if addr == "" {
		addr = ":" + DefaultPort
	}

	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	return srv.Serve(l)
}

func (srv *Server) ListenAndServeTLS(certFile, keyFile string) error {
	addr := srv.Addr
	if addr == "" {
		addr = ":" + DefaultPort
	}

	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	return srv.ServeTLS(l, certFile, keyFile)
}
